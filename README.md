# tv

Stream links from [Free-TV/IPTV on github](https://github.com/Free-TV/IPTV/blob/master/lists/germany.md)
(That repo is awesome, check it out)

A Little personal m3u8 playlist for german television channels.

![Screenshot of the playlist playing in mpv on SwayWM](screenshot.png)
